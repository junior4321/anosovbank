create table Users (iduser serial primary key,
				   name text NOT NULL,
				   surname text,
				   patronyc text,
				   money integer default 0
				   );

create table login (idlogin serial primary key,
					login text unique NOT NULL,
					password text NOT NULL,
					users integer NOT NULL references Users (iduser)
				   );

create table logs (idlogs serial primary key,
					idsend integer not null references Users (IdUser),
				  	moneysend integer not null,
				  	idget integer not null references Users (IdUser),
				  	data timestamp not null);