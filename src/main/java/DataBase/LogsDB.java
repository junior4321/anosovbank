package DataBase;

import Class.Logs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;


public class LogsDB {

    public static ArrayList<Logs> Operations (String login) {
        ArrayList<Logs> log = new ArrayList<>();
        try  {
            Connection connection = DataBase.ConnectDB.connection();
            // Получаем все строки, где в таблице logs есть записи с текущим логином
            PreparedStatement query = connection.prepareStatement("select idsend,moneysend,idget,data " +
                    "from logs " +
                    "where (idsend=(select iduser from users where iduser=(select users from login where login=?))) " +
                    "or(idget=(select iduser from users where iduser=(select users from login where login=?))) " +
                    "order by logs.data DESC ");
            query.setString(1,login);
            query.setString(2,login);
            ResultSet result = query.executeQuery();
            while (result.next()){
                // Каждую строку из полученного select записываем как новый обьект класса Logs, а после добавляем его
                // в массив ArrayList
                Logs us = new Logs(
                        // Метод Name, обьявленный в этом же классе, получает логин пользователя по его уникальному
                        // идентификатору, который используется в таблице "logs" в базе данных
                        Name(result.getInt(1)),
                        Integer.toString(result.getInt(2)),
                        Name(result.getInt(3)),
                        result.getString(4)
                );
                log.add(us);
            }
            // Меняем часть данных в массиве log, чтобы при выводе на странице main, пользователя было удобно его читать
            log = Table(log,login);

        } catch (Exception e) {e.printStackTrace();}
        return log;
    }


    public static ArrayList<Logs> Table (ArrayList<Logs> operations, String login){

        for (int i=0;i<operations.size();i++){
            // Если id отправителя и id получателя совпадают, то это значит что происходило пополнение счетаю
            // В таком случае меняем запись текущего обьекта, так, чтобы отображалось, что это пополнение счета.
            if ((operations.get(i).getIdsend().equals(login))&&(operations.get(i).getIdget().equals(login))){
                operations.get(i).setIdsend("Пополнение");
                operations.get(i).setIdget("");
            // Если id отправителя совпадает с логином, то это значит, что происходил перевод средств кому то от текущего пользователя.
            // В таком случае меняем запись текущего обьекта, так, чтобы отображалось, что это перевод средств от текущего
            // пользователя другому пользователю, а значение Balance умножаем на -1, чтобы было понятнее читать.
            } else if (operations.get(i).getIdsend().equals(login)){
                operations.get(i).setIdsend("Кому");
                operations.get(i).setMoneysend( Integer.toString(Integer.parseInt(operations.get(i).getMoneysend())*-1));
            // Если id получателя совпадает с логином, то это значит, что происходил перевод стредств от кого то к текущему пользователю.
            // В таком случае меняем запись текущего обьекта, так, чтобы отображалось, что это перевод средств от другого
            // пользователя к текущему пользователю.
            } else {
                operations.get(i).setIdget(operations.get(i).getIdsend());
                operations.get(i).setIdsend("От кого");
            }
        }
        return operations;
    }



    public static String Name (int id){
        String name=null;
        Connection connection = DataBase.ConnectDB.connection();
        try {
            PreparedStatement query = connection.prepareStatement("select login " +
                    "from login " +
                    "where users=(select iduser from users where iduser=?)");
            query.setInt(1,id);
            ResultSet result = query.executeQuery();
            while (result.next()){
                name = result.getString(1);
            }
        } catch (Exception e){e.printStackTrace();}
        return name;
    }
}
