package DataBase;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLDataException;
import java.sql.SQLException;

public class Refill {

    public static boolean refill (String login, String sum)  {
        Boolean check;
        try {
            Connection connection = DataBase.ConnectDB.connection();
            try {
                connection.setAutoCommit(false);
                // Пополняем текущее значение баланса
                PreparedStatement query = connection.prepareStatement("update users " +
                        "set money=money+? " +
                        "where iduser=(select users from login where login=?)");
                query.setInt(1, Integer.parseInt(sum));
                query.setString(2, login);
                query.executeUpdate();

                // Записываем в таблицу logs операцию пополнения счета
                query = connection.prepareStatement("insert into logs (idsend,moneysend,idget,data) " +
                        " values ((select users from login where login=?)" +
                        ",?" +
                        ",(select users from login where login=?)" +
                        ",current_timestamp)");
                query.setString(1, login);
                query.setInt(2, Integer.parseInt(sum));
                query.setString(3, login);
                query.executeUpdate();
                connection.commit();
                query.close();

                check = true;
            } catch (Exception e) {
                connection.rollback();
                e.printStackTrace();
                check = false;
            }
        } catch (SQLException e){
            e.printStackTrace();
            check=false;}
        return check;
    }
}
