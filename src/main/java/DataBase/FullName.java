package DataBase;

import java.io.IOException;
import java.sql.*;

public class FullName {

    public static String Full (String Login){
        Connection connection = DataBase.ConnectDB.connection();
        String Name="Error - Проблемы с базой данных";
        try {
            PreparedStatement query = connection.prepareStatement(
                    "select concat_ws (' ',name,surname,patronyc) " +
                            "from users " +
                            "where iduser=" +
                            "(select users from login where login=?);");
            query.setString(1,Login);
            ResultSet result = query.executeQuery();
            while (result.next()) {
                 Name = result.getString(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Возвращаем или ФИО текущего пользователя, или "Error - Проблемы с базой данных", если возникли проблемы
        return Name;
    }
}
