package DataBase;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Registration {


    public static boolean CheckLogin (String login) {
        Connection connection = DataBase.ConnectDB.connection();
        Boolean check=true;
        try {
            PreparedStatement query = connection.prepareStatement("select login from login where login=?");
            query.setString(1,login);
            ResultSet result = query.executeQuery();
            while (result.next()){
                // Если будет пройдена хоть 1 итерация цикла, то значит, что такой логин уже существует
                check=false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            check=false;}
        return check;
    }


    public static boolean Registr (String login, String password, String name, String surname, String patronyc){
        Connection connection = DataBase.ConnectDB.connection();
        // Если в метод пришли пустые значения, в те переменные, где они запрещены, то присваеваем им значения null,
        // чтобы при попытке выполнить запрос на добавление новой записи в БД, возникала ошибка, так как на уровне БД
        // реализован запрет на null значения в столбцах login,password,name
        if (login=="") {login=null;};
        if (password=="") {password=null;};
        if (name=="") {name=null;};
        Boolean check = false;
        int id=-1;
        PreparedStatement query;
        ResultSet result;

        try {
            // Добавляем в таблицу Users нового пользователя, и сразу же возвращаем уникальный идентификатор новой записи
            // и записываем его в переменную id
            query = connection.prepareStatement("insert into users (name,surname,patronyc) " +
                    "values (?,?,?) " +
                    "returning iduser");
            query.setString(1,name);
            query.setString(2,surname);
            query.setString(3,patronyc);
            result = query.executeQuery();
            while (result.next()) {
                id = result.getInt(1);
                // Отмечаем, что запрос выполнился успешно, для дальнейшей работы
                check = true; }
        } catch (Exception e){ e.printStackTrace();}

            // Если предыдущий запрос выполнился успешно, то, выполняем следующий запрос на добавление нового
            // пользователя в таблицу login.
            if (check==true) {
                try {
                check = false;
                query = connection.prepareStatement("insert into login (login,password,users) " +
                        "values (?,?,?)" +
                        "returning idlogin");
                query.setString(1,login);
                query.setString(2,password);

                // В столбец users записываем значение уникального идентификатора, полученное в предыдущем
                // запросе и записанное в переменную id
                query.setInt(3,id);
                result = query.executeQuery();

                while (result.next()) {
                    check = true; }

                // Если возникла ошибка, при добавлении новой записи в таблицу login, то не только пишем ошибку,
                // но и удаляем ранее сделанную запись в таблице users, по уникальному идентификатору, записанному в переменную id
            } catch (Exception e){
                    e.printStackTrace();
                    try {
                        query = connection.prepareStatement("delete from users where iduser=?");
                        query.setInt(1,id);
                        query.execute();
                        System.out.println("delete"); } catch (Exception t) {t.printStackTrace();}
                    }
            }

        return check;
    }
}
