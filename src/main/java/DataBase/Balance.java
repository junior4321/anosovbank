package DataBase;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class Balance {

    public static String Balance (String Login){
        Connection connection = DataBase.ConnectDB.connection();
        String money="Error - Проблемы с базой данных";
        try {
            PreparedStatement query = connection.prepareStatement(
                    "select money " +
                            "from users " +
                            "where iduser=" +
                            "(select users from login where login=?);");
            query.setString(1,Login);
            ResultSet result = query.executeQuery();
            while (result.next()) {
                money = result.getString(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Возвращаем или баланс текущего пользователя, или "Error - Проблемы с базой данных", если возникли проблемы
        return money;
    }
}
