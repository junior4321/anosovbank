package DataBase;



import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;


public class Login {



    public static int Check (String login,String pass) {
        Connection connection = DataBase.ConnectDB.connection();
        int check = 1;
        if (connection != null) {
            try {
                PreparedStatement query = connection.prepareStatement("select iduser,name " +
                        "from users " +
                        "where iduser = (select users from login where login=? and password=?)");
                query.setString(1, login);
                query.setString(2, pass);
                ResultSet res = query.executeQuery();

                /* Если будет пройдена хоть 1 итерация цикла (в любом случае будет пройдена максимум только 1 итерация,
                на уровен БД поставлено ограничение, что столбец login уникален),
                то проверка пройдена, в БД найдены соответствующие логин/пароль
                */
                while (res.next()) {
                    check = 2; // Проверка пройдена
                }
                res.close();
                query.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Connect fail");
            check = 0; // Неудачная попытка подключения к БД
        }
        return check;
    }
}
