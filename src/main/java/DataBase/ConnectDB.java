package DataBase;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class ConnectDB {

    public static Connection connection() {
        // Осуществляем подключение к PostgreSQL
        // Ip/port - 127.0.0.1:5432
        // Название БД - anosovbank
        // Имя пользователя/пароль - postgres/123
        String url = "jdbc:postgresql://127.0.0.1:5432/anosovbank";
        String name = "postgres";
        String password = "123";
        Connection connection = null;
        try {
            Class.forName("org.postgresql.Driver");
            System.out.println("Load driver");
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            connection = DriverManager.getConnection(url, name, password);
            System.out.println("Connect data");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return connection;

    }

}
