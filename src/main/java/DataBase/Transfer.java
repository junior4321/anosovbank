package DataBase;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Transfer {


    public static boolean CheckBalance (String login,String sum) {
        Connection connection = DataBase.ConnectDB.connection();
        Boolean check = false;
        int id=0;
            try {
                PreparedStatement query = connection.prepareStatement("Select money " +
                        " from users " +
                        " where iduser=(select users from login where login=?)");
                query.setString(1,login);
                ResultSet result = query.executeQuery();
                while (result.next()){
                    id = result.getInt(1);}
                if (id>Integer.parseInt(sum)) {
                    check = true;
                }
            } catch (Exception e){e.printStackTrace();}
        return check;
    }

    public static boolean CheckLogin (String login){
        Connection connection = DataBase.ConnectDB.connection();
        Boolean check = false;
        try {
            PreparedStatement query = connection.prepareStatement("select login " +
                    "from login " +
                    "where login=?");
            query.setString(1,login);
            ResultSet result = query.executeQuery();
            while (result.next()){
                check = true;
            }
        } catch (Exception e){e.printStackTrace();}
    return check;
    }


    public static boolean Transfer (String login, String sum, String sendlogin){
        Boolean check;
        try {
            Connection connection = DataBase.ConnectDB.connection();
            try {
                connection.setAutoCommit(false);

                // Отнимаем у текущего пользователя отправляемую сумму
                PreparedStatement query = connection.prepareStatement("update users " +
                        "set money=money-? " +
                        "where iduser=(select users from login where login=?)");
                query.setInt(1, Integer.parseInt(sum));
                query.setString(2, login);
                query.executeUpdate();

                // Прибавляем сумму отправляемому пользователю
                query = connection.prepareStatement("update users " +
                        "set money=money+? " +
                        "where iduser=(select users from login where login=?);");
                query.setInt(1, Integer.parseInt(sum));
                query.setString(2, sendlogin);
                query.executeUpdate();

                // Записываем данные о переводе в таблицу Logs
                query = connection.prepareStatement("insert into logs (idsend,moneysend,idget,data) " +
                        " values ((select users from login where login=?)" +
                        ",?" +
                        ",(select users from login where login=?)" +
                        ",current_timestamp)");
                query.setString(1, login);
                query.setInt(2, Integer.parseInt(sum));
                query.setString(3, sendlogin);
                query.executeUpdate();
                connection.commit();
                check = true;

            } catch (Exception e) {
                connection.rollback();
                e.printStackTrace();
                check = false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            check = false;
        }
        return check;
    }

}
