package Class;

public class Logs {
    private String idsend;
    private String moneysend;
    private String idget;
    private String data;

    public Logs(String idsend, String moneysend, String idget, String data) {
        this.idsend = idsend;
        this.moneysend = moneysend;
        this.idget = idget;
        this.data = data;
    }

    public String getIdsend() {
        return idsend;
    }

    public void setIdsend(String idsend) {
        this.idsend = idsend;
    }

    public String getMoneysend() {
        return moneysend;
    }

    public void setMoneysend(String moneysend) {
        this.moneysend = moneysend;
    }

    public String getIdget() {
        return idget;
    }

    public void setIdget(String idget) {
        this.idget = idget;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }


}


