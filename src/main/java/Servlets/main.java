package Servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;

import Class.Logs;

public class main extends HttpServlet {


    public void doPost (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();
        String login = (String) session.getAttribute("login");

        //Присваем атрибуту "login", который передадим в main.jsp, текущий логин пользователя
        request.setAttribute("login",login);
        //Присваем атрибуту "FullName", который передадим в main.jsp, ФИО текущего пользователя, который узнаем
        // при помощи метода Full в классе FullName в пакете DataBase
        request.setAttribute("FullName",DataBase.FullName.Full(login));
        //Присваем атрибуту Balance", который передадим в main.jsp, текущий баланс пользователя, который узнаем
        // при помощи метода Balance в классе Balance в пакете DataBase
        request.setAttribute("Balance", DataBase.Balance.Balance(login));

        // Создаем новый массив обьектов класса Logs (этот класс написан в пакете Class), в который при помощи
        // метода DataBase.LogsDB.Operations передаем список изменений баланса пользователя.
        ArrayList<Logs> log = DataBase.LogsDB.Operations(login);

        if (log.size()==0) {
            // Если у пользователя еще нет операция со счетом, или возникла ошибка
            request.setAttribute("check","error");
            getServletContext().getRequestDispatcher("/main.jsp").forward(request, response);
        } else {
            //Присваем атрибуту "logs", который передадим в main.jsp, список операций со счетом текущего пользователя
            request.setAttribute("logs",log);
            getServletContext().getRequestDispatcher("/main.jsp").forward(request, response);
        }
    }

    public void doGet (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String login = (String) session.getAttribute("login");

        //Если нет авторизированного логина (login==null), но происходит автоматическая пересылка на страницу входа
        if (login!=null){doPost(request,response);}
        else {response.sendRedirect("/anosov/vhod");}
    }

}
