package Servlets;

import DataBase.Login;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.DatabaseMetaData;


public class vhod extends HttpServlet {

    public void doPost (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //Вытаскиваем данные с полей логина/пароля
        String name = request.getParameter("username");
        String pass = request.getParameter("userpass");

        //Если логин/пароль = null, значит мы в 1 раз зашли на страницу, тогда просто обращаемся к vhod.jsp
        if ((name!=null)&&(pass!=null)){

         /* Проверка на вход - если значение Login.Check(name, pass) =
         0 - Неудачная попытка подключения к БД
         1 - Проверка на верный логин,пароль не пройдена
         2 - Проверка пройдена */
         int i = Login.Check(name, pass);
            if (i==2) {
                // Вход успешен, записываем в сессию текущий логин пользователя, и переходим на страницу main
                HttpSession session = request.getSession();
                session.setAttribute("login", name);
                response.sendRedirect("/anosov/main");

            } else if (i==1){
                // Пишем ошибку, что логин/пароль не верны
                request.setAttribute("check", "Логин или пароль не верны");
                getServletContext().getRequestDispatcher("/vhod.jsp").forward(request, response);
            } else if (i==0){
                // Пишем ошибку, что БД сейчас недоступна
                request.setAttribute("check", "Database failed connection");
                getServletContext().getRequestDispatcher("/vhod.jsp").forward(request, response); }

        } else {
            getServletContext().getRequestDispatcher("/vhod.jsp").forward(request, response);}
    }

    public void doGet (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Вытаскиваем из текущей сессии логин авторизированного пользователя, если он есть, иначе возвращается null
        HttpSession session = request.getSession();
        String login = (String) session.getAttribute("login");

        // Заход на страницу "vhod" удаляет текущую сессию пользователя.
        if (login!=null){session.removeAttribute("login");}
        doPost(request,response);
    }
}
