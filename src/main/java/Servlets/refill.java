package Servlets;

import DataBase.Refill;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class refill extends HttpServlet {

            public void doPost (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                request.setCharacterEncoding("UTF-8");

                HttpSession session = request.getSession();
                String login = (String) session.getAttribute("login");

                // Получаем значение суммы, введеное в форму на сайте
                String sum = request.getParameter("sum");

                    if (sum != null) {
                        // Проверка на то, что значение суммы, на которое должен пополниться баланс текущего пользователя >0
                        if (Integer.parseInt(sum) > 0){

                            // Выполняем пополнение счета с помощью метода DataBase.Refill.refill, если пополнение успешно,
                            // то уведомляем об этом и осуществляем автоматический переход на главную страницу, иначе пишем ошибку
                            if (Refill.refill(login, sum)) {
                                request.setAttribute("check", "check");
                                getServletContext().getRequestDispatcher("/SucessBalance.jsp").forward(request, response);
                            } else {
                                request.setAttribute("error", "Произошла ошибка. Проверьте введеные данные или попробуйте позже");
                                getServletContext().getRequestDispatcher("/refill.jsp").forward(request, response);
                            }

                        }  request.setAttribute("error", "Значение должно быть большое 0");
                            getServletContext().getRequestDispatcher("/refill.jsp").forward(request, response);

                    } else {getServletContext().getRequestDispatcher("/refill.jsp").forward(request, response);}
            }

            public void doGet (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                HttpSession session = request.getSession();
                String login = (String) session.getAttribute("login");

                //Если нет авторизированного логина (login==null), но происходит автоматическая пересылка на страницу входа
                if (login!=null){doPost(request,response);}
                else {response.sendRedirect("/anosov/vhod");}
            }
}
