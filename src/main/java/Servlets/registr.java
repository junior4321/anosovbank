package Servlets;

import javax.servlet.Registration;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class registr extends HttpServlet {

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        //Получаем данные в форм на странице сайте, о логине, пароле, ФИО
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        String name = request.getParameter("name");
        String surname = request.getParameter("surname");
        String patronyc = request.getParameter("patronyc");

        // Осуществляем проверку, существует ли уже такой логин или нет
        if (DataBase.Registration.CheckLogin(login)){
            // Регистрируем пользователя. Если все в порядке, то сообщаем об успешной регистрации,
            // и переадресуем пользователя на страницу входа
            if (DataBase.Registration.Registr(login, password, name, surname, patronyc)) {
                request.setAttribute("registr", "registr");
                getServletContext().getRequestDispatcher("/Succes.jsp").forward(request, response);
            } else {
                request.setAttribute("error", "Что то пошло не так. Проверьте введеные данные или попробуйте позже");
                getServletContext().getRequestDispatcher("/registr.jsp").forward(request, response);
            }

        } else{
            request.setAttribute("error", "Данный логин уже используется");
            getServletContext().getRequestDispatcher("/registr.jsp").forward(request, response);
        }
    }

}

