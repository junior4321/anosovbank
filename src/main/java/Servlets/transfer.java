package Servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.DatabaseMetaData;

public class transfer extends HttpServlet {

    public void doPost (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        request.setCharacterEncoding("UTF-8");
        HttpSession session = request.getSession();
        String login = (String) session.getAttribute("login");

            String sendlogin = request.getParameter("user");
            String sum = request.getParameter("sum");

            if ((sendlogin != null) && (sum !=null)) {
                // Проверка, чтобы логин получателя не был пустым и не был равен логину отправителя, чтобы значение суммы
                // было не пустым и >0
                if ((sendlogin != "") && (sum != "") && (Integer.parseInt(sum) > 0) && (!sendlogin.equals(login))) {
                    // Проверка на то, чтобы текущего пользователя было достаточно денег для перевода (баланс на счете > отправляемая сумма)
                    if (DataBase.Transfer.CheckBalance(login, sum)) {
                        // Проверка на то, что логин, которому текущий пользователь пересылает сумму, существует
                        if (DataBase.Transfer.CheckLogin(sendlogin)) {
                            // Если трансфер успешен, то уведомляем об этом и переадресуем пользователя на страницу main, иначе пишем ошибку
                            if (DataBase.Transfer.Transfer(login, sum, sendlogin)) {
                                request.setAttribute("check", "check");
                                getServletContext().getRequestDispatcher("/SucessBalance.jsp").forward(request, response);
                            } else {
                                request.setAttribute("error", "Перевод не удался. Проверьте введеные данные или попробуйте позже");
                                getServletContext().getRequestDispatcher("/transfer.jsp").forward(request, response);
                            }
                        } else {
                            request.setAttribute("error", "Такого пользователя нет");
                            getServletContext().getRequestDispatcher("/transfer.jsp").forward(request, response);
                        }
                    } else {
                        request.setAttribute("error", "Недостаточно средств");
                        getServletContext().getRequestDispatcher("/transfer.jsp").forward(request, response);
                    }
                } else {
                    request.setAttribute("error", "Неккоректно введеные данные");
                    getServletContext().getRequestDispatcher("/transfer.jsp").forward(request, response);
                }
            } else {getServletContext().getRequestDispatcher("/transfer.jsp").forward(request, response);}
     }

    public void doGet (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String login = (String) session.getAttribute("login");

        //Если нет авторизированного логина (login==null), но происходит автоматическая пересылка на страницу входа
        if (login!=null){doPost(request,response);}
        else {response.sendRedirect("/anosov/vhod");}
    }
}
