<%@ page import="Class.Logs" %>
<%@ page import="java.util.ArrayList" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<body>
<div id="main">
    <div>
        <%
            String login = (String) request.getAttribute("login");
            String FullName = (String) request.getAttribute("FullName");
            String Balance = (String) request.getAttribute("Balance");
            out.println("<h3>Имя пользователя - "+ login +"</h3>");
            out.println("<h3>ФИО - "+ FullName +"</h3>");
            out.println("<h3>Баланс счета - "+ Balance +"</h3>");
        %>
    </div>

<div id="refill">
    <form action="refill" method="post">
        <input type="submit" value = "Пополнение счета"/>
    </form>
</div>

<div id="transfer">
    <form action="transfer" method="post">
        <input type="submit" value = "Перевод"/>
    </form>
</div>

<div id="vhod">
    <form action="vhod" method="get">
        <input type="submit" value = "Выход"/>
    </form>
</div>


<div id="history">
    <div>
    <table align="left" border="1">

        <%
            String check = (String) request.getAttribute("check");
            if (check=="error"){ out.println("<h2>Операций со счетом нет, или возникла ошибка</h2>"); }
            else {
            out.println("<tr><th>Операция</th><th>Сумма</th><th>Кому</th><th>Дата</th></tr>");
            ArrayList<Logs> log = (ArrayList<Logs>) request.getAttribute("logs");
            for (int i=0;i<log.size();i++){
                out.println("<tr>"+"<td>"+log.get(i).getIdsend()+"</td>"+
                "<td>"+log.get(i).getMoneysend()+"</td>"+
                "<td>"+log.get(i).getIdget()+"</td>"+
                "<td>"+log.get(i).getData()+"</td>"+"</tr>");
                 }
            }
        %>
        </table>
    </div>


</body>
</html>